<?php 
require 'connect.php';
if(!isset($_SESSION['USER_LOGIN'])){
	header('location:login.php');
}else{
	
}

?>
<!DOCTYPE html>
<html lang="en">
   <head>
    <title>Registration Form</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/glyphicon.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/style.css"/>	
   </head>
   <body>
         <div class="navbar">
		    <ul>
			   <li><a href="index.php">Home</a></li>
			      <div class="nav_right">
			       <li><a href="register.php">Registration</a></li>
			        <?php if(isset($_SESSION['USER_LOGIN'])){
						 echo "<li><a href='logout.php'> Logout</a></li>";
						   }else{
							  echo "<li><a href='login.php'>Login</a></li>"; 
						   }
					?>
			      </div>
			</ul>
		  </div>
        <div class="container">
		    <h1 align="center">Welcome To The <b><?php echo $_SESSION['USER_EMAIL']?></b></h1>
		</div>
      <script src="js/jquery-2.1.1.min.js"></script>
  </body>
</html>