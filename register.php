<?php 
require "connect.php";
$msg = '';
if(isset($_POST['submit'])){
	$f_name = mysqli_real_escape_string($con,$_POST['f_name']);
	$l_name = mysqli_real_escape_string($con,$_POST['l_name']);
	$email = mysqli_real_escape_string($con,$_POST['email']);
	$password = mysqli_real_escape_string($con,$_POST['password']);
	$date_birth = mysqli_real_escape_string($con,$_POST['date_birth']);
	$gender = mysqli_real_escape_string($con,$_POST['gender']);
	$annual_income = mysqli_real_escape_string($con,$_POST['annual_income']);
	$occuption = mysqli_real_escape_string($con,$_POST['occuption']);
	$f_type = mysqli_real_escape_string($con,$_POST['family_type']);
	$manglink = mysqli_real_escape_string($con,$_POST['manglink']);
	$sql = "INSERT INTO register (f_name,l_name,email,password,date_birth,gender,annual_income,occuption,family_type,manglink) values(
	'$f_name','$l_name','$email','$password','$date_birth','$gender','$annual_income','$occuption','$f_type','$manglink')";
	if(mysqli_query($con,$sql)){
		$msg = "Registration Successfully";
	}else{
		$msg = "Registration failed";
	}
}
?>
<!DOCTYPE html>
<html lang="en">
   <head>
    <title>Registration Form</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css"/>	
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
   
   </head>
   <body>
        <div class="navbar">
		    <ul>
			   <li><a href="index.php">Home</a></li>
			   <div class="nav_right">
			     <li><a href="register.php">Register</a></li>
				 <li><a href="login.php">Login</a></li>
			   </div>
			</ul>
		  </div>
        <div class="container">
		    <form method="post">
			    <h3 align="center">Registration Form</h3>
				<span style='color:green;'><?php echo $msg ?></span>
			    <div class="form-group">
				    <label>First Name</label>
			        <input type="text"  name="f_name" class="form-control" required >		
				</div>
			    <div class="form-group">
				    <label>Last Name</label>
				    <input type="text"  name="l_name" class="form-control" required >
			    </div>
				<div class="form-group">
				    <label>Email</label>
			        <input type="email"  name="email" class="form-control" required >		
				</div>
			    <div class="form-group">
				    <label>Password</label>
				    <input type="password"  name="password" class="form-control" required >
			    </div>
				<div class="form-group">
				    <label>Date Of Birth</label>
			        <input type="date" placeholder="dd-mm-yyyy" name="date_birth" class="form-control"/>		
				</div>
			    <div class="form-group">
				    <label>Gender</label><br/>
				    Male : <input type="radio"  name="gender" value="Male" required >
					Female : <input type="radio"  name="gender" value="Female" required />
			    </div>
				<div class="form-group">
				    <label>Annual Income</label>
			        <input type="range" name="annual_income" step="1" id="annual_income" min="0" max="500000" />
                    <output for="annual_income"></output>				
				</div>
			    <div class="form-group">
				    <label>Occuption</label>
                    <select name="occuption" class="form-control">
					    <option value="">Select</option>
					    <option value="Private">Private Job</option>
						<option value="Government">Government Job</option>
						<option value="Bussiness">Bussiness</option>
                    </select>	
			    </div>
					<div class="form-group">
					<label>Family Type</label>
                    <select name="family_type" class="form-control">
					    <option value="">Select</option>
					    <option value="Joint">Joint Family</option>
						<option value="Nuclear">Nuclear Family</option>
                    </select>					
				</div>
			    <div class="form-group">
				    <label>Manglink</label>
				    <select name="manglink" class="form-control">
					    <option value="">Select</option>
					    <option value="Yes">Yes</option>
						<option value="No">No</option>
                    </select>	
			    </div>
				<input type="submit" name="submit" value="Saved Data"/>
			</form>
		</div>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<script>
		  $(document).ready(function () {
          $('output[for=annual_income]').html($('#annual_income').val());
          $('#annual_income').bind('change', function () {
          $('output[for=annual_income]').html($('#annual_income').val());
         });
		  });
		</script>
  </body>
</html>